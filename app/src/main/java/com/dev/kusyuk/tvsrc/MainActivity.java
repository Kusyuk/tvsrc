package com.dev.kusyuk.tvsrc;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.dev.kusyuk.tvsrc.Tabs.SlidingTabLayout;

public class MainActivity extends AppCompatActivity {
    public WebView view;

    private final static String APP_TITLE = "TVSRC";
    private final static String APP_PACKAGE_NAME = "com.dev.kusyuk.tvsrc";
    private FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Fullscreen//
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //Toolbar//
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.app_name);
        }
        //Set orientation: Portrait//
        int orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT; //lock screen orientation
        setRequestedOrientation(orientation);

        //Start Firebase eventLog//
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.VALUE, "app_open");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, bundle);

        //Check Network Status//
        if (isNetworkStatusAvialable(getApplicationContext())) {
        } else {
            makeToast(getString(R.string.note_nointernet));
        }
        ViewPager mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new PagerAdapter(getSupportFragmentManager()));//initialize sliding tab
        SlidingTabLayout mTabs = (SlidingTabLayout) findViewById(R.id.tabs);

        mTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.accentColor);
            }
        });
        mTabs.setDistributeEvenly(true);
        mTabs.setViewPager(mPager);

        //Check Network Status//
        if (!isNetworkStatusAvialable(getApplicationContext())) {
            makeToast(getString(R.string.note_checkinternet));
        } else {
            makeToast(getString(R.string.note_loading));
        }
    }

    //Check Network status//
    public static boolean isNetworkStatusAvialable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfos = connectivityManager.getActiveNetworkInfo();
            if (netInfos != null)
                if (netInfos.isConnected())
                    return true;
        }
        return false;
    }

    private void makeToast(String message) {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    //Pager adapter//
    class PagerAdapter extends FragmentPagerAdapter {

        //int icon[] = {R.drawable.tab_flow, R.drawable.tab_landmark}; //tab icon array
        String[] tabText;

        public PagerAdapter(FragmentManager fm) {
            super(fm);
            tabText = getResources().getStringArray(R.array.tabs);
        }

        @Override
        public Fragment getItem(int position) {
            // MyFragment myFragment = MyFragment.getInstance(position); //for single page
            Fragment fragment = null;

            if (position == 0) {
                fragment = new FragmentPortal();
            }
            if (position == 1) {
                fragment = new FragmentYouTube();
            }
            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {//display tab label

            return tabText[position];
        }

        @Override
        public int getCount() { //number of tabs
            return 2;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            devKusyuk(null);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Play Store promo//
    public void devKusyuk(View view) {
        android.app.AlertDialog.Builder mAlert = new android.app.AlertDialog.Builder(this);
        mAlert.setMessage(getString(R.string.support_1))
                .setTitle(getString(R.string.support_2))
                .setIcon(R.drawable.play_store)//use small icon
                .setPositiveButton(R.string.support_3, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Uri url = Uri.parse("https://play.google.com/store/apps/developer?id=kusyuk");
                        Intent appStore = new Intent(Intent.ACTION_VIEW, url);
                        if (appStore.resolveActivity(getPackageManager()) != null) {
                            startActivity(appStore);
                        }
                        dialog.dismiss();
                    }
                })
                .create();
        mAlert.show();
    }

    public void rateThis(View view) {
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message =  getString(R.string.about)
                + getString(R.string.rate_1)
                + APP_TITLE
                + getString(R.string.rate_2)
                + getString(R.string.rate_3);
        builder.setMessage(message)
                .setTitle(getString(R.string.rate_4) +" "+ APP_TITLE)
                .setIcon(this.getApplicationInfo().icon)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.rate_5), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("market://details?id=" + APP_PACKAGE_NAME)));
                        } catch (ActivityNotFoundException ignored) {
                        }

                    }
                }).setNegativeButton(getString(R.string.rate_6), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }
}

