package com.dev.kusyuk.tvsrc;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.File;
import java.net.URL;

/**
 * Created by Ryuga on 1/10/2016.
 */
public class FragmentPortal extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnLongClickListener {
    public WebView view;
    private SwipeRefreshLayout swipeView;
    private String currentUrl;
    ProgressBar mProgress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        if (container == null) {
            return null;
        }

        return inflater.inflate(R.layout.fragment_portal, container, false);
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mProgress = (ProgressBar) getActivity().findViewById(R.id.progressBar);
        mProgress.setVisibility(View.GONE);

        //WebView TvSrc's page+progress bar//
        String url = "https://m.facebook.com/tvSRC/";

        view = (WebView) getActivity().findViewById(R.id.webView_portal);
        final ViewGroup videoLayout = (ViewGroup) getActivity().findViewById(R.id.videoLayout); // Your own view, read class comments
        videoLayout.setVisibility(View.GONE);
        WebSettings w = view.getSettings();

        //Improve WebView Performance//
        view.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        w.setRenderPriority(WebSettings.RenderPriority.HIGH);
        w.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        w.setAppCacheEnabled(true);
        w.setDomStorageEnabled(true);
        w.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        w.setUseWideViewPort(true);
        w.setSaveFormData(true);

        getActivity().getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        if (Build.VERSION.SDK_INT >= 19) {
            view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }


        this.registerForContextMenu(view);   //invoke context menu
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    WebView webView = (WebView) v;

                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            if (webView.canGoBack()) {
                                webView.goBack();
                                return true;
                            }
                            break;
                    }
                }

                return false;
            }
        });
        swipeView = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipe_view);
        swipeView.setOnRefreshListener(this);
        view.getSettings().setJavaScriptEnabled(true);

        w.setLoadsImagesAutomatically(true);
        w.setUseWideViewPort(true);
        w.setJavaScriptCanOpenWindowsAutomatically(true);
        // HTML5 API flags
        w.setAppCacheEnabled(true);
        w.setCacheMode(WebSettings.LOAD_DEFAULT);
        w.setDatabaseEnabled(true);
        w.setDomStorageEnabled(true);
        w.setGeolocationEnabled(true);
        // HTML5 configuration parameters.
        w.setAppCacheEnabled(true);
        w.setDatabaseEnabled(true);
        w.setDomStorageEnabled(true);
        w.setGeolocationEnabled(true);
        w.setCacheMode(WebSettings.LOAD_DEFAULT);
        w.setJavaScriptCanOpenWindowsAutomatically(true);
        w.setSupportMultipleWindows(true);

        CookieManager.getInstance().setAcceptCookie(true);
        view.setWebViewClient(new WebViewController());
        view.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                mProgress.setVisibility(View.VISIBLE);
                mProgress.setProgress(newProgress);
                super.onProgressChanged(view, newProgress);
            }
            // Add new webview in same window
            @Override
            public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {
                videoLayout.setVisibility(View.VISIBLE);
                WebView childView = new WebView(view.getContext());
                childView.getSettings().setJavaScriptEnabled(true);
                childView.setWebChromeClient(this);
                childView.setWebViewClient(new WebViewController());
                childView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
                videoLayout.addView(childView);
                WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                transport.setWebView(childView);
                resultMsg.sendToTarget();
                return true;
            }

            // remove new added webview whenever onCloseWindow gets called for new webview.
            @Override
            public void onCloseWindow(WebView window) {
                videoLayout.removeViewAt(videoLayout.getChildCount() - 1);
                super.onCloseWindow(window);
            }
        });
        view.setOnLongClickListener(this);
        view.loadUrl(url);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public boolean onLongClick(View v) {
        WebView webView = (WebView) v;
        WebView.HitTestResult hr = webView.getHitTestResult();
        int type = hr.getType();
        if (type == WebView.HitTestResult.IMAGE_TYPE || type == WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE) {

            String imageUrl = hr.getExtra();
            if (!imageUrl.startsWith("http")) {
                return false;
            }
            File file = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), getFilenameFromURL(imageUrl));
            DownloadManager downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(imageUrl));
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);
            request.setDestinationUri(Uri.fromFile(file)); //save to private external storage
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, Uri.fromFile(file).getLastPathSegment()); //save to public external storage
            request.allowScanningByMediaScanner(); //scan external storage for image
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            downloadManager.enqueue(request);
            Toast.makeText(getActivity(), "Downloading Image..", Toast.LENGTH_LONG).show();
            return true;
        }
        return false;
    }

    protected String getFilenameFromURL(URL url) {
        return getFilenameFromURL(url.getFile());
    }

    protected String getFilenameFromURL(String url) {
        String[] p = url.split("/");
        String s = p[p.length - 1];
        if (s.indexOf("?") > -1) {
            return s.substring(0, s.indexOf("?"));
        }
        return s;
    }

    //To refresh page//
    @Override
    public void onRefresh() {
        view.loadUrl("javascript:window.location.reload( true )");
    }

    public class WebViewController extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            swipeView.setRefreshing(false);
            mProgress.setVisibility(View.GONE);
            super.onPageFinished(view, url);

        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            view.loadData(
                    "<div>Please check your internet connection.</div>",
                    "text/html", "UTF-8");
        }
    }
}
