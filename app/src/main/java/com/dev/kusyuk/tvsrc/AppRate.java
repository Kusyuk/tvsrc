package com.dev.kusyuk.tvsrc;

/**
 * Created by Ryuga on 1/31/2016.
 */
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

public class AppRate {

    private final static String APP_TITLE="DROPSHIP";
    private final static String APP_PACKAGE_NAME = "com.kaidi.ryuga.bookcafeshare";

    // Initialize alert dialogue //
    private final static int DAYS_UNTIL_PROMPT = 4; // number of days before prompting
    private final static int LAUNCH_UNTIL_PROMPT = 3; // number of launching before prompting

    public static void app_launched(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("rate_app", 0);
        if (prefs.getBoolean("dontShowAgain", false)) {
            return;
        }
        SharedPreferences.Editor editor = prefs.edit();
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("lauch_count", launch_count);

        long date_firstlaunch = prefs.getLong("date_first_launch", 0);
        if (date_firstlaunch == 0) {
            date_firstlaunch = System.currentTimeMillis();
            editor.putLong("date_first_launch", date_firstlaunch);
        }
        if (launch_count >= LAUNCH_UNTIL_PROMPT) {
            if (System.currentTimeMillis() >= date_firstlaunch + (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
                showRateDialog(context, editor);
            }
        }
        editor.apply();
    }
    public static void showRateDialog(final Context context, final SharedPreferences.Editor editor){
        Dialog dialog=new  Dialog (context);
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        String message = "If you enjoy using "
                + APP_TITLE
                + ", please take a moment to rate this app."
                + "Thank you for your support!";
        builder.setMessage(message)
                .setTitle("RATE " + APP_TITLE)
                .setIcon(context.getApplicationInfo().icon)
                .setCancelable(false)
                .setPositiveButton("Rate Now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editor.putBoolean("dontShowAgain", true);
                        editor.commit();
                        try {
                            context.startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("market://details?id="+APP_PACKAGE_NAME)));
                        }catch (ActivityNotFoundException e){
                            Toast.makeText(context,"You have pressed Rate Now Button",Toast.LENGTH_SHORT).show();
                        }
                    }
                }).setNeutralButton("Later", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(context,"You have pressed Later Button",Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        }).setNegativeButton("No, thanks", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (editor!=null){
                    editor.putBoolean("dontShowAgain",true);
                    editor.commit();
                }
                Toast.makeText(context,"You have pressed Rate Now Button",Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog=builder.create();
        dialog.show();

    }
}